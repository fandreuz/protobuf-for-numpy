# Serialization of NumPy arrays

Small benchmark to decide what to use to serialize/deserialize NumPy arrays.

## Protobuf

Protobuf message type:
```
message NumPyMessage {
    required bytes array = 1;
    required string dtype = 2;
}
```

The field `dtype` is needed to make sure the receiver knows what to do with the
payload. For this prototype I considered only 1D arrays, but we could add an
additional field to specify comma-separated sizes for each axis.

```python
def protobuf_round_trip(array: np.ndarray) -> np.ndarray:
    message = numpymessage_pb2.NumPyMessage()
    message.array = array.tobytes()
    message.dtype = np.lib.format.dtype_to_descr(array.dtype)
    serialized = message.SerializeToString()

    deserialized = numpymessage_pb2.NumPyMessage()
    deserialized.ParseFromString(serialized)
    dtype = np.lib.format.descr_to_dtype(deserialized.dtype)
    return np.frombuffer(deserialized.array, dtype=dtype)
```

## Avro

The field `dtype` serves the same purpose of the homonym for Protobuf.

```python
schema = avro.schema.parse(open("numpy.avsc", "rb").read())

def avro_round(array: np.ndarray) -> np.ndarray:
    # TODO: Can we serialize in-memory without writing to a file?
    with DataFileWriter(open("numpy.avro", "wb"), DatumWriter(), schema) as writer:
        writer.append({"dtype": np.lib.format.dtype_to_descr(array.dtype), "array": array.tobytes()})

    with DataFileReader(open("numpy.avro", "rb"), DatumReader()) as reader:
        deserialized = next(reader)
        dtype = np.lib.format.descr_to_dtype(deserialized["dtype"])
        return np.frombuffer(deserialized["array"], dtype=dtype)
```

## `.npy`

NumPy provides serialization to the file system with `np.save(file, array)`:

```python
def npy_round(array: np.ndarray) -> np.ndarray:
    np.save("temp.npy", array)
    return np.load("temp.npy")
```

## Current UCAP-Python approach: JSON

```python
def json_round(array: np.ndarray -> np.ndarray:
    python_value = [float(value) for value in array]
    serialized = json.dumps({"array": python_value})
    return json.loads(serialized)["array"]
```

## Results (random floats)

![](res/float_results.png)

## Limit

NumPy's standard methods only limitation is that the array should fit in memory,
while the Protobuf way stops working for some size between `2**27` and `2**28`.
The Protobuf limit apparently is not influenced by the `dtype`, since the limit
is the same for `int` and `float` arrays.

## Message size

Protobuf looks slightly cheaper than 
[NumPy's format](https://numpy.org/doc/stable/reference/generated/numpy.lib.format.html#module-numpy.lib.format).

Not sure how Avro can be constant at 59 bytes...

![](res/message_size.png)
